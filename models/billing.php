<?php 
/**
 * 
 */
class Billing extends Mnbaa_Billing_DB {
	
	protected static $table_name 				= 'billing';
	protected static $active_status_condition 	= ' active_status=0 AND category_id IN (SELECT id FROM billing_category WHERE active_status=0)';
	
	

	public static function report($from, $to, $type) {
		global $wpdb;
		$sql = "(SELECT * FROM " . static::$table_name . "
		 WHERE blog_id = " . get_current_blog_id() ." 
		 AND dateTime BETWEEN '" . $from . "' AND '" . $to . "' ";
		 $sql .= ($type) ? " AND category_id IN (SELECT id FROM billing_category WHERE type='" . $type . "')" : '' ;
		 $sql .="
		 AND parent_id='0' AND " . static::$active_status_condition . " ORDER BY id ".$_SESSION['sort_type']. " LIMIT 20)
		 UNION
		 (SELECT * FROM billing WHERE 
		 blog_id = " . get_current_blog_id() ." AND parent_id IN (
		 SELECT id FROM " . static::$table_name . "
		 WHERE blog_id = " . get_current_blog_id() ." 
		 AND dateTime BETWEEN '" . $from . "' AND '" . $to . "' 
		 AND parent_id='0' AND " . static::$active_status_condition . " ORDER BY id ".$_SESSION['sort_type']. "  
		 ))";
		return $wpdb->get_results( $sql );
	}
	
	public static function defualt_report() {
		global $wpdb;
		$sql = "(SELECT * FROM " . static::$table_name . "
		 WHERE blog_id = " . get_current_blog_id() ." 
		 AND parent_id='0' AND " . static::$active_status_condition . "  ORDER BY id ".$_SESSION['sort_type']. " LIMIT 20)
		 UNION
		 (SELECT * FROM " . static::$table_name . " WHERE 
		 blog_id = " . get_current_blog_id() ." AND parent_id IN (
		 SELECT id FROM " . static::$table_name . "
		 WHERE blog_id = " . get_current_blog_id() ." 
		 AND parent_id='0' AND " . static::$active_status_condition . " ORDER BY id ".$_SESSION['sort_type']. "
		 ))";
		return $wpdb->get_results( $sql );
	}
	
	public static function get_billing_tree($parent_id) {
		global $wpdb;
		$sql = "SELECT * FROM " . static::$table_name . " WHERE parent_id=" . $parent_id . " OR id=" . $parent_id . "";
		return $wpdb->get_results( $sql );
	}
	
	public static function sum_billing($parent_id) {
		global $wpdb;
		$sql = "SELECT SUM(value) as sum FROM " . static::$table_name . " WHERE parent_id=" . $parent_id . " OR id=" . $parent_id . "";
		return $wpdb->get_row( $sql );
	}
	
	
}
?>