<?php

function RunPlugin() {
	add_action('admin_menu', 'Mnbaa_billing_add_category_page');
    add_action('admin_menu', 'Mnbaa_billing_biiling_sub_menu');
    add_action('admin_menu', 'Mnbaa_billing_report_sub_menu');
	// add_action('admin_init','myStartSession');
	add_action('wp_ajax_billing_cat_id', 'billing_cat_id');
	add_action('wp_ajax_set_session', 'set_session');
	add_action('wp_ajax_get_cat_type', 'get_cat_type');
	add_action('wp_ajax_add_returned', 'add_returned');
	
	// load styles & scripts
	add_action( 'admin_enqueue_scripts', 'Mnbaa_billing_admin_init' );
	
	// load languages
	add_action( 'plugins_loaded', 'Mnbaa_billing_textdomain' );
}

function Mnbaa_billing_add_category_page(){
	add_menu_page( 'add category form', __('Category','billing'), 'Mnbaa_manage_billing', 'category', 'Mnbaa_billing_category_page','');
       
}

function Mnbaa_billing_biiling_sub_menu(){
	add_submenu_page(
          'category'   //or 'options.php' 
        , 'category' 
        , __('Billing','billing')
        , 'Mnbaa_manage_billing'
        , 'billing'
        , 'Mnbaa_billing_billing_page'
   	);
}

function Mnbaa_billing_report_sub_menu(){
     add_submenu_page( 
          'category'   //or 'options.php' 
        , 'category' 
      , __('Reports','billing')
        , 'Mnbaa_manage_billing'
        , 'report'
        , 'Mnbaa_billing_report_page'
    );
}

// Category page
function Mnbaa_billing_category_page(){
	include( plugin_dir_path( __DIR__ ) . 'controllers/category.php');
}

/////////////////////////////////////////// Billing ///////////////////////////////////////////
function Mnbaa_billing_billing_page(){
	include( plugin_dir_path( __DIR__ ) . 'controllers/billing.php');
}

/////////////////////////////////////////// Report ///////////////////////////////////////////
function Mnbaa_billing_report_page(){
	include( plugin_dir_path( __DIR__ ) . 'controllers/report.php');
}

// 
// ///////////////////////////////////////////////////
// function myStartSession() {
    	// session_start();
    	// // $_SESSION['sort_type'] = "ASC";
// }
// function myEndSession() {
    // session_destroy ();
// }


function Mnbaa_billing_admin_init() {
	//load jquery-ui date picker
	wp_enqueue_script('jquery-ui-datepicker');
	wp_enqueue_style('jquery-style', 'http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.2/themes/smoothness/jquery-ui.css');
	wp_enqueue_script('expanded_table', plugins_url('', __DIR__) . '/views/js/expanded_table.js');
	
	//load ajax files
	wp_register_script("ajax", WP_PLUGIN_URL . '/billing/views/js/ajax.js', array('jquery'));
	wp_localize_script('ajax', 'myAjax', array('ajaxurl' => admin_url('admin-ajax.php')));
	wp_enqueue_script('ajax');
	
	//load style file
	wp_enqueue_style('style', plugins_url('', __DIR__) . '/views/styles/style.css');
}

/**
 * Load plugin textdomain.
 *
 * @since 1.0.0
 */
function Mnbaa_billing_textdomain() {
	//load text domain file for translating
	load_plugin_textdomain('billing', false, dirname(plugin_basename(__DIR__)) . '/languages/');
	// load_plugin_textdomain( 'my-plugin', false, dirname( plugin_basename( __DIR__ ) ) . '/langs/' ); 
}

?>