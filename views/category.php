<form action="" method="post" enctype="multipart/form-data">
	<?php if ($_GET['id']): ?>
		<input type="hidden" name="id" value="<?php echo $object->id?>" />
	<?php endif ?>
	<input type="hidden" name="atributes[blog_id]" value="<?php echo get_current_blog_id();?>" />
	<table class="form-table">
		<tr>
			<th>
				<?php label(__('Parent Category','billing'), "atributes[parent_id]") ?>
			</th>
			<td>
				<?php $this_object= Billing_Category::find_by_id($_GET['id'])?>
				<select name="atributes[parent_id]"  id="cats">
					<option value="0">- - -</option>
				<?php
				$objects = Billing_Category::find_all();
				foreach ($objects as $obj):
					if ($obj->id != $_GET['id']): ?>
					<option value="<?php echo $obj->id ?> " <?php if($obj->id==$this_object->parent_id){echo "selected";}?>><?php echo $obj->name ?></option>
					<?php endif;
				endforeach ?>
				</select>
			</td>
		</tr>
                
		<tr>
			<th>
				<?php label(__('Name','billing'), "atributes[name]") ?>
			</th>
			<td>
				<?php input("atributes[name]", $object -> name) ?>
			</td>
		</tr>
		<tr id="cat_type">
			<th>
				<?php label(__('Type','billing'), "atributes[type]") ?>
			</th>
			<td>
				<select name="atributes[type]" >
					<option value="0">- - -</option>
					<option value="in" <?php if($object->type=='in'){echo "selected";}?>><?php echo __('IN','billing');?></option>
					<option value="out" <?php if($object->type=='out'){echo "selected";}?>><?php echo __('OUT','billing');?></option>
					
				</select>
			</td>
		</tr>
		<tr>
			<th>
				<?php label(__('Description','billing'), "atributes[description]") ?>
			</th>
			<td>
				<?php textarea("atributes[description]", $object -> description) ?>
			</td>
		</tr>
	</table>
	<p class="submit">
		<input type="submit" name="Submit" id="button" value="<?php echo __('Save','billing');?>" class="button button-primary" /></p>
</form>
<hr />
<table border="0" cellspacing="5" cellpadding="5">
	<tr>
		<th><?php echo __("ID",'billing');?></th>
		<th><?php echo __('Title','billing');?></th>
		<th><?php echo __('Delete','billing');?></th>
	</tr>
	<?php foreach (Billing_Category::find_all() as $obj): ?>
	<tr>
		<td><?php echo $obj->id?></td>
		<td><a href="admin.php?page=<?php echo $_REQUEST['page'];?>&id=<?php echo $obj->id?>"><?php echo $obj->name?></a></td>
		<td><a href="admin.php?page=<?php echo $_REQUEST['page'];?>&delete=<?php echo $obj->id?>"><h3>X</h3></a></td>
	</tr>
	<?php endforeach ?>
</table>