jQuery(document).ready(function() {
	/////////// Table Expanded //////////////
	var $ = jQuery;
	$(function() {

		$('select#sort').change(function(event) {
			$("tbody").each(function(elem, index) {
				var arr = $.makeArray($("tr", this).detach());
				arr.reverse();
				$(this).append(arr);
				$("tbody tr").each(function() {
					var arrange = $(this).attr('class-id');

					$('.child' + arrange).insertAfter('.parent' + arrange);
				});
			});

		});
	});

	$(function() {
		//alert($("tr[class^=child] td"));
		$("tr[class^=child] td").find("p").hide();
		$("table.report").click(function(event) {
			// event.stopPropagation();
			var $target = $(event.target);
			var parent_id = $target.closest("td").attr('data-id');

			$("tr[class^=child] td[data-id=" + parent_id + "] p").slideToggle();

		});

	});

	$(".pay_back").on('click', function(event) {
		event.stopPropagation();
		var parent_id = $(this).attr('parent-id');
		$(".back_form_"+parent_id).slideToggle();
	});

	/////////// Date Picker //////////////
	$(function() {
		$("#from").datepicker({
			defaultDate : "+1w",
			changeMonth : true,
			onClose : function(selectedDate) {
				$("#to").datepicker("option", "minDate", selectedDate);
			}
		});
		$("#to").datepicker({
			defaultDate : "+1w",
			changeMonth : true,
			onClose : function(selectedDate) {
				$("#from").datepicker("option", "maxDate", selectedDate);
			}
		});
	});

	///////////validation on date time  when submit search form//////////
	$("form.period").submit(function(event) {
		if ($("#from").val() == "" || $("#to").val() == "") {
			$("#valid").text("Enter empty fields").show();
			return false;
		}

		$("#valid").text("Valid!").show().fadeOut(1000);

	});
	//////////////////////////////////////////
	function view_form() {
		alert(id);
	}

	// $("#pay_back").click(function(event) {
	// //alert("kk");
	// if (event.target.id != "picker"){
	// $("#picker").fadeOut();
	// }
	// });
	//event.target
	/*$("#pay_back").click(function(){

	 $(".back_form").slideToggle();
	 });*/

});

