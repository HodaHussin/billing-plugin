<form action="" method="post" enctype="multipart/form-data">
	<?php if ($_GET['id']): ?>
		<input type="hidden" name="id" value="<?php echo $object->id?>" />
	<?php endif ?>
	<input type="hidden" name="atributes[blog_id]" value="<?php echo get_current_blog_id(); ?>" />
	<table class="form-table">
		<tr>
			<th>
				
				<?php label(__('Category','billing'), "atributes[category_id]") ?>
			</th>
			<td>
				
				<select name="atributes[category_id]" id="cat">
					<option value="0">- - -</option>
				<?php
				$objects = Billing_Category::find_all();
				foreach ($objects as $obj):
					 ?>
					<option value="<?php echo $obj->id ?> " <?php if ($obj->id == $object->category_id) {echo "selected";}?>>
						<?php echo $obj->name ?>
					</option>
					<?php
				endforeach?>
				</select>
			</td>
		</tr>
         <tr id="child">
         	<?php if ($_GET['id']): ?>
			<th>
				<?php label(__('Billing','billing'), "atributes[parent_id]") ?>
			</th>
			<td>
				<?php //$this_object= Billing::find_by_id($_GET['id'])?>
				<select name="atributes[parent_id]">
					<option value="0">- - -</option>
				<?php
				$objects = Billing::find_by_parent_id(0);
				foreach ($objects as $obj):
					if ($obj->id != $_GET['id']): ?>
					<option value="<?php echo $obj->id ?> " <?php if ($obj -> id == $object -> parent_id) {echo "selected";}?>>
						<?php echo $obj->title ?>
					</option>
					<?php
					endif;
				endforeach?>
				</select>
			</td>
			<?php endif ?>
		</tr>
		<tr>
			<th>
				<?php label(__('Title','billing'), "atributes[title]") ?>
			</th>
			<td>
				<?php input("atributes[title]", $object -> title) ?>
			</td>
		</tr>
    	<tr>
			<th>
				<?php label(__('Value','billing'), "atributes[value]") ?>
			</th>
			<td>
				<?php input("atributes[value]", $object -> value) ?>
			</td>
		</tr>
		
		<tr>
			<th>
				<?php label(__('Description','billing'), "atributes[description]") ?>
			</th>
			<td>
				<?php textarea("atributes[description]", $object -> description) ?>
			</td>
		</tr>
	</table>
	<p class="submit">
		<input type="submit" name="Submit" id="button" value="<?php echo __('Save','billing');?>" class="button button-primary" /></p>
</form>
<hr />
<table border="0" cellspacing="5" cellpadding="5">
	<tr>
		<th><?php echo __("ID", 'billing'); ?></th>
		<th><?php echo __('name', 'billing'); ?></th>
		<th><?php echo __('Delete', 'billing'); ?></th>
	</tr>
	<?php foreach (Billing::find_limited(10, 0) as $obj): ?>
	<tr>
		<td><?php echo $obj->id?></td>
		<td><a href="admin.php?page=<?php echo $_REQUEST['page']; ?>&id=<?php echo $obj->id?>"><?php echo $obj->title?></a></td>
		<td><a href="admin.php?page=<?php echo $_REQUEST['page']; ?>&delete=<?php echo $obj->id?>"><h3>X</h3></a></td>
	</tr>
	<?php endforeach ?>
</table>