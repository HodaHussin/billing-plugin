<input type="hidden" name="blog_id" value="<?php echo get_current_blog_id();?>" />
<form class="period" action="" method="post">
	<?php echo __('From','billing')?>:<input type="text" id="from" name="from" value=""/>
	<?php echo __('To','billing')?>:<input type="text" id="to" name="to" value=""/>
	
	<?php echo __('Type','billing')?>:
	<select name="billing_type">
		<option value="in"><?php echo __('IN', 'billing'); ?></option>
		<option value="out"><?php echo __('OUT', 'billing'); ?></option>
		<option value="0" selected><?php echo __('Both', 'billing'); ?></option>
	</select>
	<input type="submit" value="<?php echo __('Search', 'billing'); ?>">
	<span  style="display: none;" id="valid"></span>
</form>

<!-- =========================== quick search ============================== -->
<!-- Today form -->
<form action="" method="post">
	<input type="hidden" id="from" name="from" value="<?php echo date("Y-m-d")?> 00:00:00"/>
	<input type="hidden" id="to" name="to" value="<?php echo date("Y-m-d H:i:s")?>"/>
	<input type="submit" value="<?php echo __('Today', 'billing'); ?>">
</form>

<!-- Yesterday form -->
<form action="" method="post">
	<input type="hidden" id="from" name="from" value="<?php echo date("Y-m-d", strtotime("-1 day"))?> 00:00:00"/>
	<input type="hidden" id="to" name="to" value="<?php echo date("Y-m-d H:i:s")?>"/>
	<input type="submit" value="<?php echo __('Yesterday', 'billing'); ?>">
</form>

<!-- Current Week form -->
<?php
// Week Starts On (user change it from wordpress panel)
$first_day = get_option('start_of_week', $default);
$days = sort_week_days($first_day);

$overDayes = $days[date('N')];
$week_start = date('Y-m-d', strtotime('-' . ($overDayes - 1) . ' day', time()));
?>
<form action="" method="post">
	<input type="hidden" id="from" name="from" value="<?php echo $week_start?> 00:00:00"/>
	<input type="hidden" id="to" name="to" value="<?php echo date("Y-m-d H:i:s")?>"/>
	<input type="submit" value="<?php echo __('Current Week', 'billing'); ?>">
	<!--<span> <?php echo $week_start?> 00:00:00 => <?php echo date("Y-m-d H:i:s")?></span>-->
</form>

<!-- Last Week form -->
<?php
$overDayes = $days[date('N')];
$week_start = date('Y-m-d', strtotime('-' . ($overDayes + 6) . ' day', time()));
$week_end = date('Y-m-d', strtotime('-' . $overDayes . ' day', time()));
?>
<form action="" method="post">
	<input type="hidden" id="from" name="from" value="<?php echo $week_start?> 00:00:00"/>
	<input type="hidden" id="to" name="to" value="<?php echo $week_end?> 23:59:59"/>
	<input type="submit" value="<?php echo __('Last Week', 'billing'); ?>">
	<!--<span> <?php echo $week_start?> 00:00:00 => <?php echo $week_end?> 23:59:59</span>-->
</form>

<!-- Last 30 days form -->
<form action="" method="post">
	<input type="hidden" id="from" name="from" value="<?php echo date("Y-m-d", mktime(0, 0, 0, date("m", strtotime("-1 month")), 1, date("Y", strtotime("-1 month"))))?> 00:00:00"/>
	<input type="hidden" id="to" name="to" value="<?php echo date("Y-m-d", mktime(0, 0, 0, date("m", strtotime("-1 month")), date("t", strtotime("-1 month")), date("Y", strtotime("-1 month"))))?> 00:00:00"/>
	<input type="submit" value="<?php echo __('Last Month', 'billing'); ?>">
	<!--<span> <?php echo date("Y-m-d", mktime(0, 0, 0, date("m", strtotime("-1 month")), 1, date("Y", strtotime("-1 month"))))?> 00:00:00 => <?php echo date("Y-m-d", mktime(0, 0, 0, date("m", strtotime("-1 month")), date("t", strtotime("-1 month")), date("Y", strtotime("-1 month"))))?> 00:00:00</span>-->
</form>

<select id="sort">
	<option value="ASC"><?php echo __('ASC', 'billing'); ?></option>
	<option value="DESC" <?php
	if ($_SESSION['sort_type'] == 'DESC')
		echo "selected";
 ?>><?php echo __('DESC', 'billing'); ?></option>
</select>
<table class="report" cellspacing="0" cellpadding="0">
	<tr>
		<th>
			<p><?php echo __('Title', 'billing'); ?></p>
		</th>
		<th>
			<p><?php echo __('Category', 'billing'); ?></p>
		</th>
		<th>
			<p><?php echo __('DateTime', 'billing'); ?></p>
		</th>
		<th class="value">
			<p><?php echo __('Value', 'billing'); ?></p>
		</th> 
		<th class="value">
			<p><?php echo __('Add Payback', 'billing'); ?></p>
		</th>
	</tr>
	<?php
	$total;
	foreach ($parents as $parent) {
		$total = 0;
		$total += $parent->value;
		$type = Billing_Category::find_by_id($parent->category_id)->type;
		?>
	<tr class="parent_tr_<?php echo $type?> <?php
	if (!empty(Billing::find_by_parent_id($parent -> id)))
		echo "has_child";
		?>  parent<?php echo $parent->id?>" class-id="<?php echo $parent->id?>" >
		<td data-id="<?php echo $parent->id?>">
			<p> <?php echo $parent->title?> </p>
		</td>
		<td data-id="<?php echo $parent->id?>">
			<p> <?php echo Billing_Category::find_by_id($parent->category_id)->name ?> </p>
			
		</td>
		<td data-id="<?php echo $parent->id?>">
			<p> <?php echo date('Y-m-d', strtotime($parent->dateTime)) ?> </p>
		</td>
		<td data-id="<?php echo $parent->id?>">
			<p> <?php echo $parent->value ?> </p>
		</td>
		<td data-id="<?php echo $parent->id?>">
			<p> <input type="button" value="add_bill" class="pay_back" parent-id="<?php echo $parent->id?>" /> </p>
		</td>
	<!--	-->
	</tr>
	<tr style="display:none" class="back_form_<?php echo $parent->id?>" parent-id="id<?php echo $parent->id?>"> 
		<td id="small_bill" colspan="5" style="background: red">
			<p>
				<input name="title" style="width: 200px;" placeholder="title" />
				<input name="value" style="width: 100px;" placeholder="value" />
				<textarea name="description" style="width: 500px;" placeholder="description"></textarea>
			</p>
			<input type="button" value="save" class="back_form_submit" parent-id="<?php echo $parent->id?>" onclick="insert_returned($(this).attr('parent-id'))" />
		</td>
	</tr>
		<?php
		foreach ($childs as $k => $child) {
			if($child->parent_id == $parent->id){
				$total += $child->value;
				?>
	<tr class="child_tr_<?php echo $type?> child<?php echo $parent->id?> " class-id="<?php echo $parent->id?>">
		<td data-id="<?php echo $parent->id?>" >
			<p> <?php echo $child->title?> </p>
		</td>
		<td data-id="<?php echo $parent->id?>">
			<p> <?php echo Billing_Category::find_by_id($parent->category_id)->name ?> </p>
		</td>
		<td data-id="<?php echo $parent->id?>">
			<p> <?php echo date('Y-m-d H:i:s', strtotime($parent->dateTime)) ?> </p>
		</td>
		<td data-id="<?php echo $parent->id?>">
			<p> <?php echo $child->value ?> </p>
		</td>
		<td>
		</td>
		
	</tr>
				<?php
				unset($childs[$k]);
			}
		}
		?>
	<tr class="total_tr">
		<td colspan="5">
			<p> <?php echo $total ?> </p>
		</td>
	</tr>
	
		<?php
		}
	?>
</table>


<script>
	function insert_returned(id) {
		
		//alert(jQuery("#id" + id).find("input[name='returned_title']").val());
		var parent_ID 	= id;
		// var category_id	= jQuery("#id" + id).find("input[name='category_id']").val();
		var title 		= $('tr[parent-id=id'+parent_ID+']').find("input[name='title']").val();
		var value 		= $('tr[parent-id=id'+parent_ID+']').find("input[name='value']").val();
		var description	= $('tr[parent-id=id'+parent_ID+']').find("textarea").val();
		var blog_id		= jQuery("input[name='blog_id']").val();

		jQuery.ajax({
			type : "post",
			url : myAjax.ajaxurl,
			data : {
				action : "add_returned",
				title		: title,
				value		: value,
				description	: description,
				parent_id 	: parent_ID,
				blog_id		: blog_id,
			},
			success : function(response) {
				// $(".back_form_"+parent_ID).slideToggle();
				$(".back_form_"+parent_ID).slideToggle();
				$('tr[parent-id=id'+parent_ID+']').find("input[name='title']").val('');
				$('tr[parent-id=id'+parent_ID+']').find("input[name='value']").val('');
				$('tr[parent-id=id'+parent_ID+']').find("textarea").val('');
				jQuery(response).insertAfter(".child"+parent_ID+":last");
				
			}
		});

	}
</script>