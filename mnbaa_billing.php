<?php
//load models
include (plugin_dir_path(__FILE__) . 'models/db.php');
include (plugin_dir_path(__FILE__) . 'models/category.php');
include (plugin_dir_path(__FILE__) . 'models/billing.php');

//load helper
include (plugin_dir_path(__FILE__) . 'helpers/mnbaa_functions.php');
include (plugin_dir_path(__FILE__) . 'helpers/wp_functions.php');

//load  contoller file which called by ajax
include (plugin_dir_path(__FILE__) . 'controllers/ajax_functions.php');

// aactivation hook to install database when activate plugin

//include (plugin_dir_path(__FILE__) . '/activation_hook.php');

//call action on initialization
RunPlugin();
?>