<?php
function billing_db_install() {
	//echo "in";
	global $wpdb;
	require_once (ABSPATH . 'wp-admin/includes/upgrade.php');
	$sql = "CREATE TABLE IF NOT EXISTS billing_category (
  id INT(11) default NULL auto_increment,
  name VARCHAR(128) NOT NULL,
  parent_id INT(11) NOT NULL,
  blog_id INT(11) NULL,
  type enum('in','out') NOT NULL,
  description TEXT NOT NULL,
  active_status TINYINT(1) NULL,
  PRIMARY KEY (id)
) ENGINE=InnoDB
";
	dbDelta($sql);

	$sql = "CREATE TABLE IF NOT EXISTS billing (
  id INT(11) default NULL auto_increment,
  title VARCHAR(128) NOT NULL,
  description TEXT NOT NULL,
  value INT(11) NULL,
  category_id INT(11) NULL,
  parent_id INT(11) NULL,
  dateTime datetime  NULL,
  blog_id INT(11) NULL,
  PRIMARY KEY (id)
) ENGINE=InnoDB
";
	dbDelta($sql);
}
register_activation_hook(__FILE__, 'billing_db_install');
?>